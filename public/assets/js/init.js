document.addEventListener("DOMContentLoaded", function(event) {
var input, filter, ul, li, a, i, j, div;

function searchUser() {
    for (input = document.getElementById("serachChatUser"), filter = input.value.toUpperCase(), ul = document.querySelector(".chat-room-list"), li = ul.getElementsByTagName("li"), i = 0; i < li.length; i++) {
        -1 < li[i].querySelector("p").innerText.toUpperCase().indexOf(filter) ? li[i].style.display = "" : li[i].style.display = "none"
    }
}

function searchContacts() {
    for (input = document.getElementById("searchContact"), filter = input.value.toUpperCase(), list = document.querySelector(".sort-contact"), li = list.querySelectorAll(".mt-3 li"), div = list.querySelectorAll(".mt-3 .contact-list-title"), j = 0; j < div.length; j++) {
        var e = div[j];
        txtValue = e.innerText, -1 < txtValue.toUpperCase().indexOf(filter) ? div[j].style.display = "" : div[j].style.display = "none"
    }
    for (i = 0; i < li.length; i++) contactName = li[i], txtValue = contactName.querySelector("h5").innerText, -1 < txtValue.toUpperCase().indexOf(filter) ? li[i].style.display = "" : li[i].style.display = "none"
}

function searchContactOnModal() {
    for (input = document.getElementById("searchContactModal"), filter = input.value.toUpperCase(), list = document.querySelector(".contact-modal-list"), li = list.querySelectorAll(".mt-3 li"), div = list.querySelectorAll(".mt-3 .contact-list-title"), j = 0; j < div.length; j++) {
        var e = div[j];
        txtValue = e.innerText, -1 < txtValue.toUpperCase().indexOf(filter) ? div[j].style.display = "" : div[j].style.display = "none"
    }
    for (i = 0; i < li.length; i++) contactName = li[i], txtValue = contactName.querySelector("h5").innerText, -1 < txtValue.toUpperCase().indexOf(filter) ? li[i].style.display = "" : li[i].style.display = "none"
}

function getLocation() {
    navigator.geolocation ? navigator.geolocation.getCurrentPosition(showPosition) : x.innerHTML = "Geolocation is not supported by this browser."
}

function showPosition(e) {
    x.innerHTML = "Latitude: " + e.coords.latitude + "<br>Longitude: " + e.coords.longitude
}

function cameraPermission() {
    navigator.mediaDevices.getUserMedia ? navigator.mediaDevices.getUserMedia({
        video: !0
    }).then(function(e) {
        video.srcObject = e
    }).catch(function(e) {
        console.log(e)
    }) : console.log("No")
}

function audioPermission() {
    navigator.mediaDevices.getUserMedia({
        audio: !0
    }).then(function(e) {
        window.localStream = e, window.localAudio.srcObject = e, window.localAudio.autoplay = !0
    })
}

function themeColor(e) {
    var c = window.localStorage.getItem("color"),
        d = window.localStorage.getItem("image");
    document.querySelectorAll(".theme-img , .theme-color").forEach(function(r) {
        r.id == c && (r.checked = !0), r.id == d && (r.checked = !0);
        var e, t, a, s = document.querySelector("input[name=bgcolor-radio]:checked");
        s && (s = s.id, e = document.getElementsByClassName(s), t = window.getComputedStyle(e[0], null).getPropertyValue("background-color"), a = document.querySelector(".user-chat-overlay"), "bgcolor-radio8" == s ? (t = "#4eac6d", a.style.background = null) : a.style.background = t, rgbColor = t.substring(t.indexOf("(") + 1, t.indexOf(")")), document.documentElement.style.setProperty("--bs-primary-rgb", rgbColor));
        var i, l, n = document.querySelector("input[name=bgimg-radio]:checked");
        n && (n = n.id, window.localStorage.setItem("image", n), i = document.getElementsByClassName(n), e && (l = window.getComputedStyle(i[0], null).getPropertyValue("background-image"))), r.addEventListener("click", function(e) {
            r.id == c && (r.checked = !0), r.id == d && (r.checked = !0);
            var t, a, s, i = document.querySelector("input[name=bgcolor-radio]:checked");
            i && (i = i.id, (t = document.getElementsByClassName(i)) && (a = window.getComputedStyle(t[0], null).getPropertyValue("background-color"), s = document.querySelector(".user-chat-overlay"), "bgcolor-radio8" == i ? (a = "#4eac6d", s.style.background = null) : s.style.background = a, rgbColor = a.substring(a.indexOf("(") + 1, a.indexOf(")")), document.documentElement.style.setProperty("--bs-primary-rgb", rgbColor), window.localStorage.setItem("color", i)));
            var l, n, o = document.querySelector("input[name=bgimg-radio]:checked");
            o && (o = o.id, window.localStorage.setItem("image", o), l = document.getElementsByClassName(o), t && (n = window.getComputedStyle(l[0], null).getPropertyValue("background-image"), document.querySelector(".user-chat").style.backgroundImage = n))
        })
    })
}! function() {
    var d = !1,
        u = "users-chat",
        n = "assets/images/users/user-dummy-img.jpg",
        m = "users",
        s = window.location.origin + "/doot/layouts/assets/js/dir/",
        f = "",
        v = 1;

    function a() {
        var a = document.getElementsByClassName("user-chat");
        document.querySelectorAll(".chat-user-list li a").forEach(function(e) {
            e.addEventListener("click", function(e) {
                a.forEach(function(e) {
                    e.classList.add("user-chat-show")
                });
                var t = document.querySelector(".chat-user-list li.active");
                t && t.classList.remove("active"), this.parentNode.classList.add("active")
            })
        }), document.querySelectorAll(".sort-contact ul li").forEach(function(e) {
            e.addEventListener("click", function(e) {
                a.forEach(function(e) {
                    e.classList.add("user-chat-show")
                })
            })
        }), document.querySelectorAll(".user-chat-remove").forEach(function(e) {
            e.addEventListener("click", function(e) {
                a.forEach(function(e) {
                    e.classList.remove("user-chat-show")
                })
            })
        })
    }
    // document.getElementById("copyClipBoard").style.display = "none", document.getElementById("copyClipBoardChannel").style.display = "none";

    function e(e, t) {
        var a = new XMLHttpRequest;
        a.open("GET", s + e, !0), a.responseType = "json", a.onload = function() {
            var e = a.status;
            t(200 === e ? null : e, a.response)
        }, a.send()
    }

    function o() {
        "users" == m ? (document.getElementById("users-chat").style.display = "block") : (document.getElementById("users-chat").style.display = "none")
    }
     o();
    var t = document.querySelector(".user-profile-sidebar");
    document.querySelectorAll(".user-profile-show").forEach(function(e) {
        e.addEventListener("click", function(e) {
            t.classList.toggle("d-block")
        })
    }), window.addEventListener("DOMContentLoaded", function() {
        var e = document.querySelector("#chat-conversation .simplebar-content-wrapper");
        e.scrollTop = e.scrollHeight
    });
    var i = document.getElementById("chatinputmorecollapse");

    function p(e) {
        var t = document.getElementById(e).querySelector("#chat-conversation .simplebar-content-wrapper"),
            a = document.getElementsByClassName("chat-conversation-list")[0] ? document.getElementById(e).getElementsByClassName("chat-conversation-list")[0].scrollHeight - window.innerHeight + 250 : 0;
        a && t.scrollTo({
            top: a,
            behavior: "smooth"
        })
    }
    document.body.addEventListener("click", function() {
        new bootstrap.Collapse(i, {
            toggle: !1
        }).hide()
    }), i && i.addEventListener("shown.bs.collapse", function() {
        new Swiper(".chatinput-links", {
            slidesPerView: 3,
            spaceBetween: 30,
            breakpoints: {
                768: {
                    slidesPerView: 4
                },
                1024: {
                    slidesPerView: 6
                }
            }
        })
    }), document.querySelectorAll(".contact-modal-list .contact-list li").forEach(function(e) {
        e.addEventListener("click", function() {
            e.classList.toggle("selected")
        })
    });

    var l = document.querySelector("#chatinput-form"),
        g = document.querySelector("#chat-input"),
        y = document.querySelector(".chat-conversation-list");
    document.querySelector(".chat-input-feedback");

    function h() {
        var e = 12 <= (new Date).getHours() ? "pm" : "am",
            t = 12 < (new Date).getHours() ? (new Date).getHours() % 12 : (new Date).getHours(),
            a = (new Date).getMinutes() < 10 ? "0" + (new Date).getMinutes() : (new Date).getMinutes();
        return t < 10 ? "0" + t + ":" + a + " " + e : t + ":" + a + " " + e
    }
    setInterval(h, 1e3);
    var b, x, r, w = 0,
        S = [],
        E = 1;
    
    var q, k, c, L = [],
        A = 1;
    document.querySelector("#attachedfile-input").addEventListener("change", function() {
        var a = document.querySelector(".file_Upload");
        c = document.querySelector("#attachedfile-input").files[0], fr = new FileReader, fr.readAsDataURL(c), a && a.classList.add("show"), fr.addEventListener("load", function() {
            var e = c.name,
                t = Math.round(c.size / 1e6).toFixed(2);
            a.innerHTML = '<div class="card p-2 border attchedfile_pre d-inline-block position-relative">            <div class="d-flex align-items-center">                <div class="flex-shrink-0 avatar-xs ms-1 me-3">                    <div class="avatar-title bg-soft-primary text-primary rounded-circle">                        <i class="ri-attachment-2"></i>                    </div>                </div>                <div class="flex-grow-1 overflow-hidden">                <a href="" id="a"></a>                    <h5 class="font-size-14 text-truncate mb-1">' + e + '</h5>                    <input type="hidden" name="downloaddata" value="' + c + '"/>                    <p class="text-muted text-truncate font-size-13 mb-0">' + t + 'mb</p>                </div>                <div class="flex-shrink-0 align-self-start ms-3">                    <div class="d-flex gap-2">                        <div>                        <i class="ri-close-line text-muted attechedFile-remove"  id="remove-attechedFile"></i>                        </div>                    </div>                </div>            </div>          </div>', q = e, k = t, L[A] = c, removeAttachedFile()
        }, !1), A++
    });
    var C = [];
    removeimg = 1;
    document.querySelector("#galleryfile-input").addEventListener("change", function() {
        var s = document.querySelector(".file_Upload");
        s.insertAdjacentHTML("beforeend", '<div class="profile-media-img image_pre"></div>');
        var i = document.querySelector(".file_Upload .profile-media-img");
        this.files && [].forEach.call(this.files, function(e) {
            if (!/\.(jpe?g|png|gif)$/i.test(e.name)) return alert(e.name + " is not an image");
            var t = new FileReader,
                a = "";
            t.addEventListener("load", function() {
                removeimg++, s && s.classList.add("show"), C.push(t.result), a += '<div class="media-img-list" id="remove-image-' + removeimg + '">          <a href="#">              <img src="' + this.result + '" alt="' + e.name + '" class="img-fluid">          </a>            <i class="ri-close-line image-remove" onclick="removeImage(`remove-image-' + removeimg + '`)"></i>          </div>', i.insertAdjacentHTML("afterbegin", a), 0
            }), t.readAsDataURL(e)
        })
    });

    // function M(e, t) {
    //     var a = C,
    //         s = document.getElementById(e).querySelector(".chat-conversation-list"),
    //         i = "";
    //     a.forEach(function(e) {
    //         i += '<div class="message-img-list">          <div>            <a class="popup-img d-inline-block" href="' + e + '" target="_blank">                <img src="' + e + '" alt="" class="rounded border" width="200" />            </a>          </div>          <div class="message-img-link">            <ul class="list-inline mb-0">              <li class="list-inline-item dropdown">                <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">                    <i class="bx bx-dots-horizontal-rounded"></i>                </a>          <div class="dropdown-menu">            <a class="dropdown-item d-flex align-items-center justify-content-between" href="' + e + '" download>Download <i class="bx bx-download ms-2 text-muted"></i></a>            <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a>            <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a>            <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a>            <a class="dropdown-item d-flex align-items-center justify-content-between delete-image" id="delete-item-' + ++w + '" href="#">Delete <i class="bx bx-trash text-muted ms-2"></i></a>          </div>        </li>      </ul>    </div>    </div>'
    //     }), null != a && (s.insertAdjacentHTML("beforeend", '<li class="chat-list right" id="chat-list-' + w + '" >        <div class="conversation-list">            <div class="user-chat-content">                <div class="ctext-wrap">                    <div class="ctext-wrap-content">                        <div class="message-img mb-0">' + i + '                    </div>                    </div>                    </div>                  <div class="conversation-name">                    <small class="text-muted time">' + h() + '</small>                    <span class="text-success check-message-icon"><i class="bx bx-check"></i></span>                </div>          </div>        </li>'), F(), y.querySelectorAll(".chat-list").forEach(function(e) {
    //         e.querySelectorAll(".delete-image").forEach(function(e) {
    //             e.addEventListener("click", function() {
    //                 1 == e.closest(".message-img").childElementCount ? e.closest(".chat-list").remove() : e.closest(".message-img-list").remove()
    //             })
    //         })
    //     }), B.querySelectorAll(".chat-list").forEach(function(e) {
    //         e.querySelectorAll(".delete-image").forEach(function(e) {
    //             e.addEventListener("click", function() {
    //                 1 == e.closest(".message-img").childElementCount ? e.closest(".chat-list").remove() : e.closest(".message-img-list").remove()
    //             })
    //         })
    //     })), document.querySelector(".file_Upload").classList.remove("show"), C = []
    // }
    // l && l.addEventListener("submit", function(e) {
    //     e.preventDefault();
    //     var t = u,
    //         a = u,
    //         s = u,
    //         i = u,
    //         l = u,
    //         n = g.value,
    //         o = document.querySelector(".image_pre"),
    //         r = document.querySelector(".attchedfile_pre"),
    //         c = document.querySelector(".audiofile_pre");
    //     null != o || null != o ? M(a) : null != r ? function(e, t, a) {
    //         t = q, a = k;
    //         w++;
    //         var s = document.getElementById(e).querySelector(".chat-conversation-list");
    //         null != t && s.insertAdjacentHTML("beforeend", '<li class="chat-list right" id="chat-list-' + w + '" >          <div class="conversation-list">              <div class="user-chat-content">                  <div class="ctext-wrap">                      <div class="ctext-wrap-content">                          <div class="p-3 border-primary border rounded-3">                              <div class="d-flex align-items-center attached-file">                                  <div class="flex-shrink-0 avatar-sm me-3 ms-0 attached-file-avatar">                                      <div class="avatar-title bg-soft-primary text-primary rounded-circle font-size-20"><i class="ri-attachment-2"></i></div>                                  </div>                                  <div class="flex-grow-1 overflow-hidden">                                      <div class="text-start">                                          <h5 class="font-size-14 mb-1">' + t + '</h5>                                          <p class="text-muted text-truncate font-size-13 mb-0">' + a + 'mb</p>                                          <p class="text-muted text-truncate font-size-13 mb-0"></p>                                      </div>                                  </div>                                  <div class="flex-shrink-0 ms-4">                                      <div class="d-flex gap-2 font-size-20 d-flex align-items-start">                                          <div>                                              <a href="#" class="text-muted download-file" data-id="' + A + '"> <i class="bx bxs-download"></i> </a>                                          </div>                                      </div>                                  </div>                              </div>                          </div>                      </div>                      <div class="dropdown align-self-start message-box-drop">                          <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="ri-more-2-fill"></i> </a>                          <div class="dropdown-menu">                          <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a>                          <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a>                          <a class="dropdown-item d-flex align-items-center justify-content-between copy-message" href="#" id="copy-message-' + w + '">Copy <i class="bx bx-copy text-muted ms-2"></i></a>                          <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a>                          <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Mark as Unread <i class="bx bx-message-error text-muted ms-2"></i></a>                          <a class="dropdown-item d-flex align-items-center justify-content-between delete-item" id="delete-item-' + w + '" href="#">Delete <i class="bx bx-trash text-muted ms-2"></i></a>                      </div>                    </div>                  </div>                <div class="conversation-name">                    <small class="text-muted time">' + h() + '</small>                      <span class="text-success check-message-icon"><i class="bx bx-check"></i></span>                    </div>                </div>              </div>            </li>');
    //         var i = document.getElementById("chat-list-" + w);
    //         i.querySelectorAll(".delete-item").forEach(function(e) {
    //             e.addEventListener("click", function() {
    //                 y.removeChild(i)
    //             })
    //         }), i.querySelectorAll(".download-file").forEach(function(i) {
    //             i.addEventListener("click", function(e) {
    //                 e.preventDefault();
    //                 var t, a, s = i.getAttribute("data-id");
    //                 window.File && window.FileReader && window.FileList && window.Blob ? (t = new Blob([L[s]], {
    //                     type: "application/pdf"
    //                 }), (a = document.createElement("a")).href = window.URL.createObjectURL(t), a.download = L[s].name, a.click()) : alert("The File APIs are not fully supported in this browser.")
    //             })
    //         }), document.querySelector(".file_Upload ").classList.remove("show")
    //     }(s, n) : null != c ? function(e, t, a) {
    //         t = b, a = x;
    //         w++;
    //         var s = document.getElementById(e).querySelector(".chat-conversation-list");
    //         null != t && s.insertAdjacentHTML("beforeend", '<li class="chat-list right" id="chat-list-' + w + '" >          <div class="conversation-list">              <div class="user-chat-content">                  <div class="ctext-wrap">                      <div class="ctext-wrap-content">                          <div class="p-3 border-primary border rounded-3">                              <div class="d-flex align-items-center attached-file">                                  <div class="flex-shrink-0 avatar-sm me-3 ms-0 attached-file-avatar">                                      <div class="avatar-title bg-soft-primary text-primary rounded-circle font-size-20"><i class="bx bx-headphone"></i></div>                                  </div>                                  <div class="flex-grow-1 overflow-hidden">                                      <div class="text-start">                                          <h5 class="font-size-14 mb-1">' + t + '</h5>                                          <p class="text-muted text-truncate font-size-13 mb-0">' + a + 'mb</p>                                  </div>                                  </div>                                  <div class="flex-shrink-0 ms-4">                                      <div class="d-flex gap-2 font-size-20 d-flex align-items-start">                                          <div>                                          <a href="#" class="text-muted download-file" data-id="' + E + '" > <i class="bx bxs-download"></i> </a>                                          </div>                                      </div>                                  </div>                              </div>                          </div>                      </div>                      <div class="dropdown align-self-start message-box-drop">                          <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="ri-more-2-fill"></i> </a>                          <div class="dropdown-menu">                          <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a>                          <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a>                          <a class="dropdown-item d-flex align-items-center justify-content-between copy-message" href="#" id="copy-message-' + w + '">Copy <i class="bx bx-copy text-muted ms-2"></i></a>                          <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a>                          <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Mark as Unread <i class="bx bx-message-error text-muted ms-2"></i></a>                           <a class="dropdown-item d-flex align-items-center justify-content-between delete-item" id="delete-item-' + w + '" href="#">Delete <i class="bx bx-trash text-muted ms-2"></i></a>                      </div>                      </div>                      </div>                      <div class="conversation-name">                          <small class="text-muted time">' + h() + '</small>                            <span class="text-success check-message-icon"><i class="bx bx-check"></i></span>                          </div>                        </div>                      </div>                    </li>');
    //         var i = document.getElementById("chat-list-" + w);
    //         i.querySelectorAll(".delete-item").forEach(function(e) {
    //             e.addEventListener("click", function() {
    //                 y.removeChild(i)
    //             })
    //         }), document.querySelectorAll(".download-file").forEach(function(i) {
    //             i.addEventListener("click", function(e) {
    //                 e.preventDefault();
    //                 var t, a, s = i.getAttribute("data-id");
    //                 window.File && window.FileReader && window.FileList && window.Blob ? (t = new Blob([L[s]], {
    //                     type: "application/mp3"
    //                 }), (a = document.createElement("a")).href = window.URL.createObjectURL(t), a.download = S[s].name, a.click()) : alert("The File APIs are not fully supported in this browser.")
    //             })
    //         }), document.querySelector(".file_Upload ").classList.remove("show")
    //     }(i, n) : 1 == d ? (function(e, t) {
    //         var a = document.querySelector(".user-profile-show").innerHTML,
    //             s = document.querySelector(".replyCard .replymessage-block .flex-grow-1 .mb-0").innerText;
    //         w++;
    //         var i = document.getElementById(e).querySelector(".chat-conversation-list");
    //         null != t && (i.insertAdjacentHTML("beforeend", '<li class="chat-list right" id="chat-list-' + w + '" >                <div class="conversation-list">                    <div class="user-chat-content">                        <div class="ctext-wrap">                            <div class="ctext-wrap-content">                            <div class="replymessage-block mb-0 d-flex align-items-start">                        <div class="flex-grow-1">                            <h5 class="conversation-name">' + a + '</h5>                            <p class="mb-0">' + s + '</p>                        </div>                        <div class="flex-shrink-0">                            <button type="button" class="btn btn-sm btn-link mt-n2 me-n3 font-size-18">                            </button>                        </div>                    </div>                                <p class="mb-0 ctext-content mt-1">                                    ' + t + '                                </p>                            </div>                            <div class="dropdown align-self-start message-box-drop">                                <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">                                    <i class="ri-more-2-fill"></i>                                </a>                                <div class="dropdown-menu">                                    <a class="dropdown-item d-flex align-items-center justify-content-between reply-message" href="#" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a>                                    <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a>                                    <a class="dropdown-item d-flex align-items-center justify-content-between copy-message" href="#" id="copy-message-' + w + '">Copy <i class="bx bx-copy text-muted ms-2"></i></a>                                    <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a>                                    <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Mark as Unread <i class="bx bx-message-error text-muted ms-2"></i></a>                                    <a class="dropdown-item d-flex align-items-center justify-content-between delete-item" id="delete-item-' + w + '" href="#">Delete <i class="bx bx-trash text-muted ms-2"></i></a>                            </div>                        </div>                    </div>                    <div class="conversation-name">                        <small class="text-muted time">' + h() + '</small>                        <span class="text-success check-message-icon"><i class="bx bx-check"></i></span>                    </div>                </div>            </div>        </li>'), 0);
    //         var l = document.getElementById("chat-list-" + w);
    //         l.querySelectorAll(".delete-item").forEach(function(e) {
    //             e.addEventListener("click", function() {
    //                 y.removeChild(l)
    //             })
    //         }), l.querySelectorAll(".copy-message").forEach(function(e) {
    //             e.addEventListener("click", function() {
    //                 document.getElementById("copyClipBoard").style.display = "block", document.getElementById("copyClipBoardChannel").style.display = "block", setTimeout(function() {
    //                     document.getElementById("copyClipBoard").style.display = "none", document.getElementById("copyClipBoardChannel").style.display = "none"
    //                 }, 1e3)
    //             })
    //         }), l.querySelectorAll(".reply-message").forEach(function(s) {
    //             s.addEventListener("click", function() {
    //                 var e = s.closest(".ctext-wrap").children[0].children[0].innerText,
    //                     t = document.querySelector(".user-profile-show").innerHTML;
    //                 document.querySelector(".replyCard .replymessage-block .flex-grow-1 .mb-0").innerText = e;
    //                 var a = !s.closest(".chat-list") || s.closest(".chat-list").classList.contains("left") ? t : "You";
    //                 document.querySelector(".replyCard .replymessage-block .flex-grow-1 .conversation-name").innerText = a
    //             })
    //         }), l.querySelectorAll(".copy-message").forEach(function(e) {
    //             e.addEventListener("click", function() {
    //                 l.childNodes[1].children[1].firstElementChild.firstElementChild.getAttribute("id"), isText = l.childNodes[1].children[1].firstElementChild.firstElementChild.innerText, navigator.clipboard.writeText(isText)
    //             })
    //         })
    //     }(l, n), d = !1) : function(e, t) {
    //         w++;
    //         var a = document.getElementById(e).querySelector(".chat-conversation-list");
    //         null != t && a.insertAdjacentHTML("beforeend", '<li class="chat-list right" id="chat-list-' + w + '" >                <div class="conversation-list">                    <div class="user-chat-content">                        <div class="ctext-wrap">                            <div class="ctext-wrap-content">                                <p class="mb-0 ctext-content">' + t + '</p>                            </div>                            <div class="dropdown align-self-start message-box-drop">                                <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">                                    <i class="ri-more-2-fill"></i>                                </a>                                <div class="dropdown-menu">                                    <a class="dropdown-item d-flex align-items-center justify-content-between reply-message" href="#" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a>                                    <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a>                                    <a class="dropdown-item d-flex align-items-center justify-content-between copy-message" href="#" id="copy-message-' + w + '">Copy <i class="bx bx-copy text-muted ms-2"></i></a>                                    <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a>                                    <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Mark as Unread <i class="bx bx-message-error text-muted ms-2"></i></a>                                    <a class="dropdown-item d-flex align-items-center justify-content-between delete-item" id="delete-item-' + w + '" href="#">Delete <i class="bx bx-trash text-muted ms-2"></i></a>                            </div>                        </div>                    </div>                    <div class="conversation-name">                        <small class="text-muted time">' + h() + '</small>                        <span class="text-success check-message-icon"><i class="bx bx-check"></i></span>                    </div>                </div>            </div>        </li>');
    //         var s = document.getElementById("chat-list-" + w);
    //         s.querySelectorAll(".delete-item").forEach(function(e) {
    //             e.addEventListener("click", function() {
    //                 a.removeChild(s)
    //             })
    //         }), s.querySelectorAll(".copy-message").forEach(function(e) {
    //             e.addEventListener("click", function() {
    //                 document.getElementById("copyClipBoard").style.display = "block", document.getElementById("copyClipBoardChannel").style.display = "block", setTimeout(function() {
    //                     document.getElementById("copyClipBoard").style.display = "none", document.getElementById("copyClipBoardChannel").style.display = "none"
    //                 }, 1e3)
    //             })
    //         }), s.querySelectorAll(".reply-message").forEach(function(i) {
    //             i.addEventListener("click", function() {
    //                 var e = document.querySelector(".replyCard"),
    //                     t = document.querySelector("#close_toggle"),
    //                     a = i.closest(".ctext-wrap").children[0].children[0].innerText,
    //                     s = document.querySelector(".user-profile-show").innerHTML;
    //                 d = !0, e.classList.add("show"), t.addEventListener("click", function() {
    //                     e.classList.remove("show")
    //                 }), document.querySelector(".replyCard .replymessage-block .flex-grow-1 .mb-0").innerText = a, document.querySelector(".replyCard .replymessage-block .flex-grow-1 .conversation-name").innerText = s
    //             })
    //         }), s.querySelectorAll(".copy-message").forEach(function(e) {
    //             e.addEventListener("click", function() {
    //                 var e = s.childNodes[1].firstElementChild.firstElementChild.firstElementChild.firstElementChild.innerText;
    //                 navigator.clipboard.writeText(e)
    //             })
    //         })
    //     }(t, n), p(t || a || s || i || l), g.value = "", document.querySelector(".image_pre") && document.querySelector(".image_pre").remove(), document.getElementById("galleryfile-input").value = "", document.querySelector(".attchedfile_pre") && document.querySelector(".attchedfile_pre").remove(), document.getElementById("attachedfile-input").value = ""
    // });
    // var B = document.querySelector("#channel-conversation");
    
    for (var j = document.getElementsByClassName("favourite-btn"), T = 0; T < j.length; T++) {
        var I = j[T];
        I.onclick = function() {
            I.classList.toggle("active")
        }
    }
    new FgEmojiPicker({
        trigger: [".emoji-btn"],
        removeOnSelection: !1,
        closeButton: !0,
        position: ["top", "right"],
        preFetch: !0,
        dir: "assets/js/dir/json",
        insertInto: document.querySelector(".chat-input")
    });

    function H(e, t, a, s, i) {
        var l = '<div class="ctext-wrap">';
        if (null != t) l += '<div class="ctext-wrap-content" id=' + e + '>        <p class="mb-0 ctext-content">' + t + "</p></div>";
        else if (a && 0 < a.length) {
            for (l += '<div class="message-img mb-0">', T = 0; T < a.length; T++) l += '<div class="message-img-list">            <div>              <a class="popup-img d-inline-block" href="' + a[T] + '">                <img src="' + a[T] + '" alt="" class="rounded border">              </a>            </div>            <div class="message-img-link">              <ul class="list-inline mb-0">                <li class="list-inline-item dropdown">                  <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">                      <i class="bx bx-dots-horizontal-rounded"></i>                  </a>                <div class="dropdown-menu">                  <a class="dropdown-item d-flex align-items-center justify-content-between" href="' + a[T] + '" download>Download <i class="bx bx-download ms-2 text-muted"></i></a>                  <a class="dropdown-item d-flex align-items-center justify-content-between"  href="#" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a>                  <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a>                  <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a>                  <a class="dropdown-item d-flex align-items-center justify-content-between delete-image" href="#">Delete <i class="bx bx-trash ms-2 text-muted"></i></a>                </div>              </li>          </ul>        </div>      </div>';
            l += "</div>"
        } else 0 < s.length && (l += '<div class="ctext-wrap-content">            <div class="p-3 border-primary border rounded-3">            <div class="d-flex align-items-center attached-file">                <div class="flex-shrink-0 avatar-sm me-3 ms-0 attached-file-avatar">                    <div class="avatar-title bg-soft-primary text-primary rounded-circle font-size-20">                        <i class="ri-attachment-2"></i>                    </div>                </div>                <div class="flex-grow-1 overflow-hidden">                    <div class="text-start">                        <h5 class="font-size-14 mb-1">design-phase-1-approved.pdf</h5>                        <p class="text-muted text-truncate font-size-13 mb-0">12.5 MB</p>                    </div>                </div>                <div class="flex-shrink-0 ms-4">                    <div class="d-flex gap-2 font-size-20 d-flex align-items-start">                        <div>                            <a href="#" class="text-muted">                                <i class="bx bxs-download"></i>                            </a>                        </div>                    </div>                </div>             </div>            </div>            </div>            <div class="dropdown align-self-start message-box-drop">                <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">                    <i class="ri-more-2-fill"></i>                </a>                <div class="dropdown-menu">                  <a class="dropdown-item d-flex align-items-center justify-content-between"  href="' + s + '" download>Download <i class="bx bx-download ms-2 text-muted"></i></a>                  <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a>                  <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a>                  <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a>                  <a class="dropdown-item d-flex align-items-center justify-content-between delete-item" href="#">Delete <i class="bx bx-trash text-muted ms-2"></i></a>                </div>            </div>');
        return !0 === i && (l += '<div class="dropdown align-self-start message-box-drop">                <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">                    <i class="ri-more-2-fill"></i>                </a>                <div class="dropdown-menu">                    <a class="dropdown-item d-flex align-items-center justify-content-between reply-message" href="#" id="reply-message-' + w + '" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a>                    <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a>                    <a class="dropdown-item d-flex align-items-center justify-content-between copy-message" href="#" id="copy-message-' + w + '">Copy <i class="bx bx-copy text-muted ms-2"></i></a>                    <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a>                    <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Mark as Unread <i class="bx bx-message-error text-muted ms-2"></i></a>                    <a class="dropdown-item d-flex align-items-center justify-content-between delete-item" href="#">Delete <i class="bx bx-trash text-muted ms-2"></i></a>                </div>            </div>'), l += "</div>"
    }

    function _(e) {
        var t, a, s;
        t = e, a = function(e, t) {
            var l, n, a, i, o, s, r, c;
            null !== e ? console.log("Something went wrong: " + e) : (l = "users" == m ? t[0].chats : t[0].channel_chat, document.getElementById(m + "-conversation").innerHTML = "", n = 0, l.forEach(function(t, e) {
                var a, s, i;
                0 < n ? --n : (a = t.from_id == v ? " right" : " left", s = f.find(function(e) {
                    return e.id == t.from_id
                }), i = '<li class="chat-list' + a + '" id=' + t.id + '>                        <div class="conversation-list">', v != t.from_id && (i += '<div class="chat-avatar"><img src="' + s.profile + '" alt=""></div>'), i += '<div class="user-chat-content">', i += H(t.id, t.msg, t.has_images, t.has_files, t.has_dropDown), l[e + 1] && t.from_id == l[e + 1].from_id && (n = function(e, t, a) {
                    for (var s = 0; e[t] && e[t + 1] && e[t + 1].from_id == a;) s++, t++;
                    return s
                }(l, e, t.from_id), i += function(e, t, a) {
                    for (var s = 0; e[t] && e[t + 1] && e[t + 1].from_id == a;) s = H(e[t + 1].id, e[t + 1].msg, e[t + 1].has_images, e[t + 1].has_files, e[t + 1].has_dropDown), t++;
                    return s
                }(l, e, t.from_id)), i += '<div class="conversation-name"><small class="text-muted time">' + t.datetime + '</small> <span class="text-success check-message-icon"><i class="bx bx-check-double"></i></span></div>', i += "</div>                </div>            </li>", document.getElementById(m + "-conversation").innerHTML += i)
            })), y.querySelectorAll(".delete-item").forEach(function(e) {
                e.addEventListener("click", function() {
                    2 == e.closest(".user-chat-content").childElementCount ? e.closest(".chat-list").remove() : e.closest(".ctext-wrap").remove()
                })
            }), B.querySelectorAll(".delete-item").forEach(function(e) {
                e.addEventListener("click", function() {
                    2 == e.closest(".user-chat-content").childElementCount ? e.closest(".chat-list").remove() : e.closest(".ctext-wrap").remove()
                })
            }), y.querySelectorAll(".chat-list").forEach(function(e) {
                e.querySelectorAll(".delete-image").forEach(function(e) {
                    e.addEventListener("click", function() {
                        1 == e.closest(".message-img").childElementCount ? e.closest(".chat-list").remove() : e.closest(".message-img-list").remove()
                    })
                })
            }), y.querySelectorAll(".copy-message").forEach(function(t) {
                t.addEventListener("click", function() {
                    var e = t.closest(".ctext-wrap").children[0] ? t.closest(".ctext-wrap").children[0].children[0].innerText : "";
                    navigator.clipboard.writeText(e)
                })
            }), B.querySelectorAll(".copy-message").forEach(function(t) {
                t.addEventListener("click", function() {
                    var e = t.closest(".ctext-wrap").children[0] ? t.closest(".ctext-wrap").children[0].children[0].innerText : "";
                    navigator.clipboard.writeText(e)
                })
            }), p("users-chat"), F(), document.querySelectorAll(".copy-message").forEach(function(e) {
                e.addEventListener("click", function() {
                    document.getElementById("copyClipBoard").style.display = "block", document.getElementById("copyClipBoardChannel").style.display = "block", setTimeout(function() {
                        document.getElementById("copyClipBoard").style.display = "none", document.getElementById("copyClipBoardChannel").style.display = "none"
                    }, 1e3)
                })
            }), a = y.querySelectorAll(".reply-message"), i = document.querySelector(".replyCard"), o = document.querySelector("#close_toggle"), a.forEach(function(s) {
                s.addEventListener("click", function() {
                    d = !0, i.classList.add("show"), o.addEventListener("click", function() {
                        i.classList.remove("show")
                    });
                    var e = s.closest(".ctext-wrap").children[0].children[0].innerText;
                    document.querySelector(".replyCard .replymessage-block .flex-grow-1 .mb-0").innerText = e;
                    var t = document.querySelector(".user-profile-show").innerHTML,
                        a = !subitem.closest(".chat-list") || subitem.closest(".chat-list").classList.contains("left") ? t : "You";
                    document.querySelector(".replyCard .replymessage-block .flex-grow-1 .conversation-name").innerText = a
                })
            }), s = B.querySelectorAll(".reply-message"), r = document.querySelector(".replyCard"), c = document.querySelector("#close_toggle"), s.forEach(function(a) {
                a.addEventListener("click", function() {
                    d = !0, r.classList.add("show"), c.addEventListener("click", function() {
                        r.classList.remove("show")
                    });
                    var e = a.closest(".ctext-wrap").children[0].children[0].innerText;
                    document.querySelector(".replyCard .replymessage-block .flex-grow-1 .mb-0").innerText = e;
                    var t = document.querySelector(".user-profile-show").innerHTML;
                    document.querySelector(".replyCard .replymessage-block .flex-grow-1 .conversation-name").innerText = t
                })
            })
        }, (s = new XMLHttpRequest).open("GET", t, !0), s.responseType = "json", s.onload = function() {
            var e = s.status;
            a(200 === e ? null : e, s.response)
        }, s.send()
    }

    function F() {
        GLightbox({
            selector: ".popup-img",
            title: !1
        })
    }
    // document.getElementById("emoji-btn").addEventListener("click", function() {
    //     setTimeout(function() {
    //         var e, t = document.getElementsByClassName("fg-emoji-picker")[0];
    //         !t || (e = window.getComputedStyle(t) ? window.getComputedStyle(t).getPropertyValue("left") : "") && (e = (e = e.replace("px", "")) - 40 + "px", t.style.left = e)
    //     }, 0)
    // })
}();
var primaryColor = window.getComputedStyle(document.body, null).getPropertyValue("--bs-primary-rgb");

function removeImage(e) {
    document.querySelector("#" + e).remove(), 0 == document.querySelectorAll(".image-remove").length && document.querySelector(".file_Upload").classList.remove("show")
}

function removeAttachedFile() {
    document.getElementById("remove-attechedFile") && (document.getElementsByClassName("attechedFile-remove")[0], document.getElementById("remove-attechedFile").addEventListener("click", function(e) {
        e.target.closest(".attchedfile_pre").remove()
    })), document.querySelector("#remove-attechedFile").addEventListener("click", function() {
        document.querySelector(".file_Upload ").classList.remove("show")
    })
}

function removeAudioFile() {
    document.getElementById("remove-audioFile") && (document.getElementsByClassName("audioFile-remove")[0], document.getElementById("remove-audioFile").addEventListener("click", function(e) {
        e.target.closest(".audiofile_pre").remove()
    })), document.querySelector("#remove-audioFile").addEventListener("click", function() {
        document.querySelector(".file_Upload ").classList.remove("show")
    })
}
themeColor(primaryColor);

});