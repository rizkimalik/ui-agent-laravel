const soundMessage = new Audio('public/assets/sound/MESSAGE.mp3');

function AskPermission() {
    try {
        Notification.requestPermission().then((permission) => {
            console.log(`Notification permission : ${permission}`);
        });
    }
    catch (e) {
        return false;
    }
    return true;
}

function ShowNotification(Title, Body) {
    function PushNotification() {
        let options = {
            body: `${Body}`,
            icon: 'public/assets/img/mendawai.png',
            tag: Body
        }
        const notif = new Notification(Title, options);
        notif.onclick = function (event) {
            event.preventDefault();
            // location.href = `${DomainUrl}/inhealth/HTML/TrxShowTicket.aspx?ticketid=${Title}`;
        }
        soundMessage.play();
    }


    if (Notification.permission === 'granted') {
        PushNotification();
    } else if (Notification.permission !== 'denied') {
        Notification.requestPermission().then((permission) => {
            if (permission === 'granted') {
                PushNotification();
            }
        });
    }
}