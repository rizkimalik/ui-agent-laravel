<x-app-master>
    <div class="bg">
        <div class="container-fluid p-0">
            <div class="row g-0">
                <div class="col-xl-9 col-lg-8">
                    <div class="authentication-page-content">
                        <div class="d-flex flex-column h-100 px-4 pt-4">
                            <div class="row justify-content-center my-auto">
                                <div class="col-sm-8 col-lg-6 col-xl-5 col-xxl-4">

                                    <div class="py-md-5 py-4">
                                        <div class="text-center mb-5">
                                            <div class="mt-10">
                                                <img src="{{ asset('assets/img/logo-hitam.png') }}" alt="" class=""
                                                    style="height: 40px;">
                                            </div>
                                            <p class="text-muted">Sign in continue to Chat.</p>
                                        </div>

                                        <form method="POST" action="{{ url('login') }}" id="form-login">
                                            @csrf
                                            <div class="mb-3">
                                                <label for="username" class="form-label">Username</label>
                                                <input type="text" class="form-control" id="username" name="username"
                                                    placeholder="Enter Username" required>
                                            </div>

                                            <div class="mb-3">
                                                <label for="password" class="form-label">Password</label>
                                                <input type="password" class="form-control" id="password" name="password"
                                                    placeholder="Enter Password" required>
                                            </div>

                                            <div class="form-check form-check-info font-size-16">
                                                <input class="form-check-input" type="checkbox" id="remember-check">
                                                <label class="form-check-label font-size-14" for="remember-check">
                                                    Remember me
                                                </label>
                                            </div>

                                            <div class="text-center mt-4">
                                                <button class="btn btn-primary w-100" id="btn-login" type="submit">Log
                                                    In</button>
                                            </div>
                                        </form>
                                        <!-- end form -->

                                        <div class="mt-5 text-center text-muted" id="status-permission">
                                            <p>Notification Block <a href="{{ url('/login') }}"
                                                    class="fw-medium text-decoration-underline"> Reload</a></p>
                                        </div>
                                    </div>
                                </div><!-- end col -->
                            </div><!-- end row -->

                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="text-center text-muted p-4">
                                        <p class="mb-0">&copy;
                                            <script>
                                                document.write(new Date().getFullYear())
                                            </script> Mendawai.
                                            <i class="mdi mdi-heart text-danger"></i> by Selindo Alpha
                                        </p>
                                    </div>
                                </div><!-- end col -->
                            </div><!-- end row -->

                        </div>
                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container-fluid -->
    </div>

    <x-slot:script>
        <script>
            function requestPermission() {
                const firebaseConfig = {
                    // apiKey: "AIzaSyBDRIiWOUF51w6m1dvdNtLkHehcjcv-C1o",
                    // authDomain: "worktool-f50cb.firebaseapp.com",
                    // projectId: "worktool-f50cb",
                    // storageBucket: "worktool-f50cb.appspot.com",
                    // messagingSenderId: "808725237125",
                    // appId: "1:808725237125:web:33a8c8ad003e2df913c8ec"
    
                    apiKey: "AIzaSyDG3DVEILnwTYdo1RPuCfaXRHoNRSyXnEM",
                    authDomain: "web-chat-a7180.firebaseapp.com",
                    projectId: "web-chat-a7180",
                    storageBucket: "web-chat-a7180.appspot.com",
                    messagingSenderId: "492304206438",
                    appId: "1:492304206438:web:0dd178e41155a8133efba7",
                };
    
                firebase.initializeApp(firebaseConfig);
                const messaging = firebase.messaging();
                
                document.getElementById("status-permission").style.display = 'none';
                messaging.requestPermission()
                    .then(() => {
                        return messaging.getToken();
                    })
                    .then((token)=>{
                        console.log("Notification permission granted.");
                        localStorage.setItem('uuid', token);
                        document.getElementById("btn-login").disabled = false;
                        document.getElementById("status-permission").style.display = 'none';
                    })
                    .catch(function (err) {
                        console.log("Notification permission blocked.", err);
                        document.getElementById("btn-login").disabled = true;
                        document.getElementById("status-permission").style.display = 'block';
                    });
            }
            requestPermission();
    
            document.getElementById('btn-login').addEventListener('click', async (e) => {
                e.preventDefault();

                const raw = JSON.stringify({
                    "uuid": localStorage.getItem('uuid'),
                    "username": document.getElementById('username').value,
                    "flag_to": 'Layer1'
                });
    
                const requestOptions = {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: raw,
                    redirect: 'follow'
                };
    
                await fetch("https://mendawai.test/api-chat/login.php", requestOptions)
                .then(response => response.json())
                .then((result) => {
                    console.log(result)
                    document.getElementById("form-login").submit();
                })
                .catch(error => console.log('error', error));
            });
    
            
        </script>
    </x-slot:script>
</x-app-master>