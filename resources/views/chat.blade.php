<x-app-master>
    <x-main-content>
        <x-conversation />
        <x-form-send />
    </x-main-content>

    <x-slot:script>
        <script src="{{ asset('assets/js/init.js') }}"></script>
        <script src="{{ asset('assets/js/notification.js') }}"></script>
        <script src="http://localhost:3001/socket.io/socket.io.js"></script>

        <script>
            /* section global parameter */
            const urlSearchParams = new URLSearchParams(window.location.search);
            const params = Object.fromEntries(urlSearchParams.entries());
            const url_api = `http://localhost:3001`;
            const formSend = document.querySelector("#chatinput-form");
            const message = document.querySelector("#chat-input");
            const conversation = document.querySelector("#chat-conversation-list");
            const submitbtn = document.querySelector("#submit-btn");
            const btn_endchat = document.querySelector("#btn-endchat");
            const customer_list = document.querySelector("#customer-list");
            const img_profile = document.querySelector("#img-profile");
            const active_name = document.querySelector("#active-name");
            const active_status = document.querySelector("#active-status");
            const total_live = document.querySelector("#total-live");
            const display_agent = document.querySelector("#display-agent");
            const display_status = document.querySelector("#display-status");
            const navbar_profile = document.querySelector("#navbar-profile");
            display_status.innerHTML = '<small class="badge badge-soft-warning rounded p-1 text-truncate mb-4">Agent not ready.</small>';

            /* section end global parameter */

            /* section socket */
            const socket = io(url_api);
            socket.auth = {
                username: params.username,
                flag_to: 'agent',
            }
            socket.connect();
            socket.on('connect', function(){
                console.info(new Date().toLocaleString() + ' : id = '+ socket.id);
                AskPermission();
                onLoginAgent(socket.id);
                setShowHideToolbar(true);
                display_status.innerHTML = '<small class="badge badge-soft-primary rounded p-1 text-truncate mb-4">Ready to chat.</small>';
            });
            

            socket.on('return-message-customer', function(data){
                const ActiveCustomer = localStorage.getItem('ActiveCustomer');
                ShowNotification(data.name, data.message);
                getCustomerList(); //load customer
                
                if (ActiveCustomer) {
                    const {customer_id} = JSON.parse(ActiveCustomer);
                    if (data.customer_id == customer_id) {
                        active_status.innerHTML = data.typing ? "<small>typing...</small>" : "<small>online</small>";
                        conversation.innerHTML += 
                            `<li class="chat-list left">
                                <div class="conversation-list">
                                    <div class="chat-avatar">
                                        <div class="avatar-sm">
                                            <div class="avatar-title text-primary bg-soft-primary rounded-circle border">
                                                <i class="bx bxs-message-square-detail"></i>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="user-chat-content">
                                        <div class="ctext-wrap">
                                            <div class="ctext-wrap-content" id="1">
                                                <p class="mb-0 ctext-content">${data.message}</p>
                                            </div>
                                            <div class="dropdown align-self-start message-box-drop"> <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-vertical-rounded align-middle"></i> </a>
                                                <div class="dropdown-menu"> <a class="dropdown-item d-flex align-items-center justify-content-between reply-message" href="#" id="reply-message-0" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between copy-message" href="#" id="copy-message-0">Copy <i class="bx bx-copy text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Mark as Unread <i class="bx bx-message-error text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between delete-item" href="#">Delete <i class="bx bx-trash text-muted ms-2"></i></a> </div>
                                            </div>
                                        </div>
                                        <div class="conversation-name">
                                            <small class="text-muted time">${h()}</small> 
                                            <span class="text-success check-message-icon"><i class="bx bx-check-double"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </li>`;
                    }
                }
            });

            socket.on('return-typing', (res) => {
                active_status.innerHTML = res.typing ? "<small>typing...</small>" : '<small class="text-muted d-block"><i class="bx bxs-circle text-success font-size-10 align-middle"></i> online</small>';
            });
            /* section end socket */

            /* section form send message */
            submitbtn.addEventListener("click", async (e) => {
                e.preventDefault();
                await onSendMessage();
            });

            message.addEventListener("keypress", async (e)=>{
                if (e.key === 'Enter') {
                    e.preventDefault();
                    await onSendMessage();
                } 
                else {
                    const ActiveCustomer = localStorage.getItem('ActiveCustomer');
                    const data = JSON.parse(ActiveCustomer);

                    let values = {
                        uuid_customer: data.uuid_customer,
                        uuid_agent: socket.id,
                        flag_to: 'agent',
                        typing: true
                    }
                    socket.emit('typing', values);
                }
            });

            btn_endchat.addEventListener("click", (e) => {
                e.preventDefault();
                Swal.fire({
                    title: 'End Chat',
                    text: 'Do you want to close the chat?',
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'End Chat',
                }).then((result) => {
                    if (result.isConfirmed) {
                        onEndChat();
                        Swal.fire('Success!', '', 'success')
                    } 
                })
            });
            /* section end form send message */

            /* section function get data */
            async function onLoginAgent(socket_id) {
                const response = await fetch(`${url_api}/sosmed/join_chat`, { 
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        uuid: socket_id,
                        username: params.username,
                        flag_to: 'agent'
                    })
                });

                const json = await response.json();
                if (json.status === 200) {
                    localStorage.removeItem('ActiveCustomer');
                    display_agent.innerHTML = params.username;
                    getCustomerList()
                }
                else{
                    alert('Login Failed.')
                }
            }

            async function onSendMessage() {
                const ActiveCustomer = localStorage.getItem('ActiveCustomer');
                const data = JSON.parse(ActiveCustomer);

                let values = {
                    chat_id: data.chat_id,
                    customer_id: data.customer_id,
                    message: message.value,
                    name: params.username,
                    email: data.email,
                    uuid_customer: data.uuid_customer,
                    uuid_agent: socket.id,
                    flag_to: 'agent',
                    agent_handle: data.agent_handle,
                    date_create: new Date(),
                    typing: false
                }
        
                if (message.value) {
                    socket.emit('send-message-agent', values);
                    socket.emit('typing', values);
				    
                    conversation.innerHTML += 
                        `<li class="chat-list right">
                            <div class="conversation-list">
                                <div class="user-chat-content">
                                    <div class="ctext-wrap">
                                        <div class="ctext-wrap-content">
                                            <p class="mb-0 ctext-content">${message.value}</p>
                                        </div>
                                        <div class="dropdown align-self-start message-box-drop"> <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-vertical-rounded align-middle"></i> </a>
                                            <div class="dropdown-menu"> <a class="dropdown-item d-flex align-items-center justify-content-between reply-message" href="#" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between copy-message" href="#">Copy <i class="bx bx-copy text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Mark as Unread <i class="bx bx-message-error text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between delete-item" href="#">Delete <i class="bx bx-trash text-muted ms-2"></i></a> </div>
                                        </div>
                                    </div>
                                    <div class="conversation-name">
                                        <small class="text-muted time">${h()}</small> 
                                        <span class="text-success check-message-icon"><i class="bx bx-check"></i></span>
                                    </div>
                                </div>
                            </div>
                        </li>`;
                    message.value = "";
                }

				return false;
            }
            
            async function getCustomerList() {
                const response = await fetch(`${url_api}/sosmed/list_customers?agent=${params.username}`, { 
                    method: "GET",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                });

                const json = await response.json();
                const data = json.data;

                if (json.status === 200) {
                    let html = '';
                    let active = '';
                    const ActiveCustomer = localStorage.getItem('ActiveCustomer');
                    const actv = JSON.parse(ActiveCustomer);
                    
                    for (let i = 0; i < data.length; i++) {
                        if (actv) {
                            active = data[i].chat_id === actv.chat_id ? 'active' : '';
                        }
                        let status_connected = data[i].connected ? '<span class="user-status"></span>' : '';

                        html += `<li class="border-bottom ${active}" id="chatid-${data[i].chat_id}"> 
                                    <a href="javascript: void(0);">
                                        <div class="d-flex align-items-center my-2">
                                            <div class="chat-user-img online align-self-center me-2 ms-0">
                                                <div class="avatar-sm">
                                                    <div class="avatar-title bg-primary rounded-circle border">
                                                        <i class="bx bxs-message-square-detail"></i>
                                                    </div>
                                                    ${status_connected}
                                                </div>
                                            </div>
                                            <div class="flex-grow-1 overflow-hidden">
                                                <span class="">${data[i].name}</span>
                                                <p class="text-truncate mb-0">${data[i].email}</p>
                                            </div>
                                            <div class="ms-auto">
                                                <span class="badge badge-soft-dark rounded p-1">${data[i].total_chat}</span>
                                            </div>
                                        </div>
                                    </a> 
                                </li>`;
                    }
                    total_live.innerHTML = data.length;
                    customer_list.innerHTML = '';
                    customer_list.innerHTML += html;

                    //? On Select Customer
                    data.map((raw, index) => {
                        document.getElementById(`chatid-${raw.chat_id}`)
                        .addEventListener('click', (e) => {
                            e.preventDefault()
                            onSelectCustomer(raw);
                        });
                    });
                }
                else{
                    console.log(json);
                }
            }
            
            const onSelectCustomer = async (data) => {
                localStorage.setItem('ActiveCustomer', JSON.stringify(data));
                active_name.innerHTML = data.name;
                active_status.innerHTML = data.connected 
                    ? '<small class="text-muted d-block"><i class="bx bxs-circle text-success font-size-10 align-middle"></i> online</small>' 
                    : '<small class="text-muted d-block"><i class="bx bxs-circle text-danger font-size-10 align-middle"></i> offline</small>';
                img_profile.classList.remove('hide');
                setShowHideToolbar(false);
                await getCustomerList();
                getConversationData(data.chat_id, data.customer_id)
            }

            async function getConversationData(chat_id, customer_id) {
                const response = await fetch(`${url_api}/sosmed/conversation_chats`, { 
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        chat_id: chat_id,
                        customer_id: customer_id,
                    })
                });

                const json = await response.json();
                const data = json.data;

                if (json.status === 200) {
                    let html = '';
                    for (let i = 0; i < data.length; i++) {
                        const read_status = data[i].flag_notif ? '<i class="bx bx-check-double"></i>' : '<i class="bx bx-check"></i>';

                        if (data[i].flag_to === 'customer') {
                            html += `<li class="chat-list left">
                                    <div class="conversation-list">
                                        <div class="chat-avatar">
                                            <div class="avatar-sm">
                                                <div class="avatar-title text-primary bg-soft-primary rounded-circle border">
                                                    <i class="bx bxs-message-square-detail"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="user-chat-content">
                                            <div class="ctext-wrap">
                                                <div class="ctext-wrap-content" id="1">
                                                    <p class="mb-0 ctext-content">${data[i].message}</p>
                                                </div>
                                            </div>
                                            <div class="conversation-name">
                                                <small class="text-muted time">${data[i].date_create}</small> 
                                                <span class="text-success check-message-icon">${read_status}</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>`;
                        } 
                        else {
                            html += `<li class="chat-list right">
                                <div class="conversation-list">
                                    <div class="user-chat-content">
                                        <div class="ctext-wrap">
                                            <div class="ctext-wrap-content">
                                                <p class="mb-0 ctext-content">${data[i].message}</p>
                                            </div>
                                        </div>
                                        <div class="conversation-name">
                                            <small class="text-muted time">${data[i].date_create}</small> 
                                            <span class="text-success check-message-icon">${read_status}</span>
                                        </div>
                                    </div>
                                </div>
                            </li>`;
                        }
                        
                    }
                    conversation.innerHTML = '';
                    conversation.innerHTML += html;
                }
            }

            const onEndChat = async () => {
                const ActiveCustomer = localStorage.getItem('ActiveCustomer');
                const data = JSON.parse(ActiveCustomer);

                const response = await fetch(`${url_api}/sosmed/end_chat`, { 
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        chat_id: data.chat_id,
                        customer_id: data.customer_id,
                    })
                });

                const json = await response.json();
                if (json.status === 200) {
                    localStorage.removeItem('ActiveCustomer');
                    active_name.innerHTML = '';
                    active_status.innerHTML = '';
                    conversation.innerHTML = '';
                    setShowHideToolbar(true);
                    await getCustomerList();
                }
            }

            const setShowHideToolbar = (value) => {
                if (value === true) {
                    formSend.classList.add('hide');
                    navbar_profile.classList.add('hide');
                } else {
                    formSend.classList.remove('hide');
                    navbar_profile.classList.remove('hide');
                }
            }

            function h() {
                var e = 12 <= (new Date).getHours() ? "pm" : "am",
                    t = 12 < (new Date).getHours() ? (new Date).getHours() % 12 : (new Date).getHours(),
                    a = (new Date).getMinutes() < 10 ? "0" + (new Date).getMinutes() : (new Date).getMinutes();
                return t < 10 ? "0" + t + ":" + a + " " + e : t + ":" + a + " " + e
            }
            /* section end function get data */

        </script>
    </x-slot:script>
</x-app-master>