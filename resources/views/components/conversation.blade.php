<div id="users-chat" class="position-relative">
    <x-topbar />

    <div class="chat-conversation p-3 p-lg-4 " id="chat-conversation" data-simplebar>
        <ul class="list-unstyled chat-conversation-list" id="chat-conversation-list"></ul>
    </div>

    {{-- {{ $slot }} --}}

    {{-- <div class="alert alert-warning alert-dismissible copyclipboard-alert px-4 fade show " id="copyClipBoard"
        role="alert">
        message copied
    </div> --}}


    <!-- end chat conversation end -->
</div>