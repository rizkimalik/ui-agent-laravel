<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"  style="--bs-primary-rgb:80, 165, 241;">

<head>

    <meta charset="utf-8" />
    <title>{{ $title }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Responsive Chat App" name="description" />
    <meta name="keywords"
        content="chat, web chat template, chat status, chat template, communication, discussion, group chat, message, messenger template, status" />
    <meta content="Mendawai" name="author" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" id="tabIcon">
    <!-- glightbox css -->
    <link rel="stylesheet" href="{{ asset('assets/css/glightbox.min.css') }}">
    <!-- swiper css -->
    <link rel="stylesheet" href="{{ asset('assets/css/swiper-bundle.min.css') }}">
    <link href="{{ asset('assets/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
    <!-- Bootstrap Css -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/sweetalert/sweetalert2.min.css') }}" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="{{ asset('assets/css/app.min.css') }}" id="app-style" rel="stylesheet" type="text/css" />

    {{ $style }}
</head>

<body>
    <div>
        {{ $slot }}
    </div>

    <!-- JAVASCRIPT -->
    <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/simplebar.min.js') }}"></script>
    <script src="{{ asset('assets/js/waves.min.js') }}"></script>
    <!-- glightbox js -->
    <script src="{{ asset('assets/js/glightbox.min.js') }}"></script>
    <!-- Swiper JS -->
    <script src="{{ asset('assets/js/swiper-bundle.min.js') }}"></script>
    <!-- fg-emoji-picker JS -->
    <script src="{{ asset('assets/js/fgEmojiPicker.js') }}"></script>
    <script src="{{ asset('assets/sweetalert/sweetalert2.all.min.js') }}"></script>
    <!-- page init -->
    {{-- <script src="{{ asset('assets/js/init.js') }}"></script> --}}
    <script src="{{ asset('assets/js/app.js') }}"></script>


    {{-- <script src="https://www.gstatic.com/firebasejs/8.10.0/firebase-app.js"></script> --}}
    {{-- <script src="https://www.gstatic.com/firebasejs/8.10.0/firebase-messaging.js"></script> --}}

    <!-- <script src="https://www.gstatic.com/firebasejs/9.9.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/9.9.1/firebase-messaging.js"></script> -->
    
    {{ $script }}

</body>

</html>