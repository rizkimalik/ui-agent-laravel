<div class="position-relative">
    <div class="chat-input-section p-3 p-lg-4">

        <form id="chatinput-form" enctype="multipart/form-data">
            <div class="row g-0 align-items-center">
                <div class="file_Upload"></div>
                <div class="col-auto">
                    <div class="chat-input-links me-md-2">
                        <div class="links-list-item" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-placement="top" title="File Document">
                            <button type="button" class="btn btn-link text-decoration-none btn-lg waves-effect"aria-expanded="false">
                                <i class="bx bx-paperclip align-middle"></i>
                            </button>
                        </div>
                        <div class="links-list-item" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-placement="top" title="File Images">
                            <button type="button" class="btn btn-link text-decoration-none btn-lg waves-effect">
                                <i class="bx bx-images align-middle"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="position-relative">
                        <div class="chat-input-feedback">
                            Please Enter a Message
                        </div>
                        <input autocomplete="off" type="text"
                            class="form-control form-control-lg chat-input" autofocus
                            id="chat-input" placeholder="Type your message...">
                    </div>
                </div>
                <div class="col-auto">
                    <div class="chat-input-links ms-2 gap-md-1">
                        <div class="links-list-item">
                            <button type="button"
                                class="btn btn-primary btn-lg chat-send waves-effect waves-light"
                                data-bs-toggle="collapse"
                                data-bs-target=".chat-input-collapse1.show">
                                <i class="bx bxs-send align-middle" id="submit-btn"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="chat-input-collapse chat-input-collapse1 collapse" id="chatinputmorecollapse">
            <div class="card mb-0">
                <div class="card-body py-3">
                    <!-- Swiper -->
                    <div class="swiper chatinput-links">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="text-center px-2 position-relative">
                                    <div>
                                        <input id="attachedfile-input" type="file" class="d-none"
                                            accept=".zip,.rar,.7zip,.pdf" multiple>
                                        <label for="attachedfile-input"
                                            class="avatar-sm mx-auto stretched-link">
                                            <span
                                                class="avatar-title font-size-18 bg-soft-primary text-primary rounded-circle">
                                                <i class="bx bx-paperclip"></i>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="text-center px-2 position-relative">
                                    <div>
                                        <input id="galleryfile-input" type="file" class="d-none"
                                            accept="image/png, image/gif, image/jpeg" multiple>
                                        <label for="galleryfile-input"
                                            class="avatar-sm mx-auto stretched-link">
                                            <span
                                                class="avatar-title font-size-18 bg-soft-primary text-primary rounded-circle">
                                                <i class="bx bx-images"></i>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide ">
                                <div class="text-center px-2">
                                    <div class="avatar-sm mx-auto">
                                        <div class="avatar-title font-size-18 bg-soft-primary text-primary rounded-circle"
                                            onclick="audioPermission()">
                                            <i class="bx bx-microphone"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="replyCard">
        <div class="card mb-0">
            <div class="card-body py-3">
                <div class="replymessage-block mb-0 d-flex align-items-start">
                    <div class="flex-grow-1">
                        <h5 class="conversation-name"></h5>
                        <p class="mb-0"></p>
                    </div>
                    <div class="flex-shrink-0">
                        <button type="button" id="close_toggle"
                            class="btn btn-sm btn-link mt-n2 me-n3 font-size-18">
                            <i class="bx bx-x align-middle"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>