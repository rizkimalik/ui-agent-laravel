<div class="chat-leftsidebar">

    <div class="tab-content">

        <!-- Start chats tab-pane -->
        <div class="tab-pane show active" id="pills-chat" role="tabpanel" aria-labelledby="pills-chat-tab">
            <!-- Start chats content -->
            <div>
                <div class="px-4 pt-4">
                    <div class="d-flex align-items-start">
                        <div class="flex-grow-1">
                            <h6 class="text-truncate mb-2 font-size-18">
                                <i class='bx bx-user-check align-middle'></i> 
                                <span id="display-agent"></span>
                            </h6>
                            <span id="display-status"></span>
                            
                        </div>
                        <div class="flex-shrink-0">
                            <div data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-placement="bottom" title="Total Live">
                                <button type="button" class="btn btn-soft-primary btn-sm">
                                    Live <span id="total-live" >0</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <form>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control bg-light border-0 pe-0" id="serachChatUser"
                                onkeyup="searchUser()" placeholder="Search here.."
                                aria-label="Example text with button addon" aria-describedby="searchbtn-addon"
                                autocomplete="off">
                            {{-- <button class="btn btn-light" type="button" id="searchbtn-addon"><i
                                    class='bx bx-search align-middle'></i></button> --}}
                        </div>
                    </form>

                </div> <!-- .p-4 -->

                <h5 class="mb-3 px-4 mt-4 font-size-11 text-muted text-uppercase">Customer List</h5>
                <div class="chat-room-list" data-simplebar>
                    <!-- Start chat-message-list -->
                    <div class="chat-message-list">
                        {{-- <ul class="list-unstyled chat-list chat-user-list" id="customer-list"> --}}
                        <ul class="list-unstyled chat-list" id="customer-list">

                            {{-- <li class="active"> 
                                <a href="javascript: void(0);">
                                    <div class="d-flex align-items-center">
                                        <div class="chat-user-img online align-self-center me-2 ms-0">
                                            <div class="avatar-xs">
                                                <span class="avatar-title rounded-circle bg-primary text-white">
                                                    <span class="username">JP</span>
                                                    <span class="user-status"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="flex-grow-1 overflow-hidden">
                                            <span class="">James Pinard</span>
                                            <p class="text-truncate mb-0">jame@mail.com</p>
                                        </div>
                                        <div class="ms-auto">
                                            <span class="badge badge-soft-dark rounded p-1">8</span>
                                        </div>
                                    </div>
                                </a> 
                            </li> --}}
                        </ul>
                    </div>

                </div>
            </div>
            <!-- Start chats content -->

        </div>
        <!-- End chats tab-pane -->

    </div>
    <!-- end tab content -->
</div>