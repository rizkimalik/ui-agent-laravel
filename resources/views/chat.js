const session = @json($session);
            const url_api = 'https://mendawai.test/api-chat';
            const formSend = document.querySelector("#chatinput-form");
            const message = document.querySelector("#chat-input");
            const conversation = document.querySelector("#chat-conversation-list");
            const submitbtn = document.querySelector("#submit-btn");
            const customer_list = document.querySelector("#customer-list");
            const active_name = document.querySelector("#active_name");
            const active_status = document.querySelector("#active_status");
            // const token_to = document.querySelector("#searchChatMessage");

            const firebaseConfig = {
                // apiKey: "AIzaSyBDRIiWOUF51w6m1dvdNtLkHehcjcv-C1o",
                // authDomain: "worktool-f50cb.firebaseapp.com",
                // projectId: "worktool-f50cb",
                // storageBucket: "worktool-f50cb.appspot.com",
                // messagingSenderId: "808725237125",
                // appId: "1:808725237125:web:33a8c8ad003e2df913c8ec"

                apiKey: "AIzaSyDG3DVEILnwTYdo1RPuCfaXRHoNRSyXnEM",
                authDomain: "web-chat-a7180.firebaseapp.com",
                projectId: "web-chat-a7180",
                storageBucket: "web-chat-a7180.appspot.com",
                messagingSenderId: "492304206438",
                appId: "1:492304206438:web:0dd178e41155a8133efba7",
            };

            firebase.initializeApp(firebaseConfig);

            const messaging = firebase.messaging();
            messaging.requestPermission()
            .then(function () {
                console.log("Notification permission granted.");
                return messaging.getToken();
                // return messaging.getToken({vapidKey: "BEuMvTP23k7cKq2w7ISif00fosMKK0TRFaQ6SuHrJvrV53U7qGEjbYHw_-7bnO08PeIU9urpYjjcVhYudUWw0ws"});
            })
            .then(function (token) {
                localStorage.setItem('uuid', token);
                LoginAgent();
            })
            .catch(function (err) {
                console.log("Unable to get permission to notify.", err);
            });

            firebase.messaging().onMessage(function (payload) {
                const data = payload.data;
                // localStorage.setItem('ActiveCustomer', JSON.stringify(data));
                getCustomerList(); //load customer
                const ActiveCustomer = localStorage.getItem('ActiveCustomer');
                if (ActiveCustomer) {
                    const {customer_id} = JSON.parse(ActiveCustomer);
                    if (data.customer_id == customer_id) {
                        conversation.innerHTML += 
                            `<li class="chat-list left">
                                <div class="conversation-list">
                                    <div class="chat-avatar">
                                        <div class="avatar-sm">
                                            <div class="avatar-title text-primary bg-soft-primary rounded-circle border">
                                                <i class="bx bxs-message-square-detail"></i>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="user-chat-content">
                                        <div class="ctext-wrap">
                                            <div class="ctext-wrap-content" id="1">
                                                <p class="mb-0 ctext-content">${data.message}</p>
                                            </div>
                                            <div class="dropdown align-self-start message-box-drop"> <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-vertical-rounded align-middle"></i> </a>
                                                <div class="dropdown-menu"> <a class="dropdown-item d-flex align-items-center justify-content-between reply-message" href="#" id="reply-message-0" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between copy-message" href="#" id="copy-message-0">Copy <i class="bx bx-copy text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Mark as Unread <i class="bx bx-message-error text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between delete-item" href="#">Delete <i class="bx bx-trash text-muted ms-2"></i></a> </div>
                                            </div>
                                        </div>
                                        <div class="conversation-name">
                                            <small class="text-muted time">${h()}</small> 
                                            <span class="text-success check-message-icon"><i class="bx bx-check-double"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </li>`;
                    }
                }


                const {title, body, icon} = payload.notification;
                const notificationOption = {
                    body: body,
                    icon: icon
                };
                navigator.serviceWorker
                    .getRegistrations()
                    .then((registration) => {
                        registration[0].showNotification(`${title}`, notificationOption);
                    });
            });

            formSend.addEventListener("submit", async (e) => {
                e.preventDefault();

                const ActiveCustomer = localStorage.getItem('ActiveCustomer');
                const data = JSON.parse(ActiveCustomer);

                const options = {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        "uuid_customer":data.uuid_customer,
                        "uuid_agent":localStorage.getItem('uuid'),
                        "uuid":data.uuid_customer,
                        "name": session.username,
                        "customer_id":data.customer_id,
                        "chat_id": data.chat_id,
                        "email":data.email,
                        "message":message.value,
                        "flag_to":"agent",
                        "agent_handle": "agent1"
                    }),
                    redirect: 'follow'
                }

                // await fetch("/chat-app/api/sendmessage", options)
                // await fetch("https://mendawai.test/api-chat/sendchat.php", options)
                await fetch(`${url_api}/send_message.php`, options)
                .then(response => response.json())
                .then(result => (result) => {
                    console.log(result)
                    if (result.success === 1) {
                        alert('sukses kirim')
                    } 
                    else {
                        alert('Error send message.')
                    }
                })
                .catch(error => console.log('error', error));

                conversation.innerHTML += 
                `<li class="chat-list right">
                    <div class="conversation-list">
                        <div class="user-chat-content">
                            <div class="ctext-wrap">
                                <div class="ctext-wrap-content">
                                    <p class="mb-0 ctext-content">${message.value}</p>
                                </div>
                                <div class="dropdown align-self-start message-box-drop"> <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-vertical-rounded align-middle"></i> </a>
                                    <div class="dropdown-menu"> <a class="dropdown-item d-flex align-items-center justify-content-between reply-message" href="#" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between copy-message" href="#">Copy <i class="bx bx-copy text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Mark as Unread <i class="bx bx-message-error text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between delete-item" href="#">Delete <i class="bx bx-trash text-muted ms-2"></i></a> </div>
                                </div>
                            </div>
                            <div class="conversation-name">
                                <small class="text-muted time">${h()}</small> 
                                <span class="text-success check-message-icon"><i class="bx bx-check"></i></span>
                            </div>
                        </div>
                    </div>
                </li>`;
                message.value = "";
                
            });

            async function LoginAgent() {
                // var username = prompt("Enter your name");
                const response = await fetch(`${url_api}/login.php`, { 
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        uuid: localStorage.getItem('uuid'),
                        username: session.username,
                        flag_to: 'Layer1'
                    })
                });

                const json = await response.json();
                if (json.status === 'success') {
                    getCustomerList();
                    localStorage.removeItem('ActiveCustomer');
                }
                else{
                    alert('Login Failed.')
                }
            }
            
            async function getCustomerList() {
                const response = await fetch(`${url_api}/customer.php?action=data&agent=${session.username}`, { 
                    method: "GET",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                });

                const json = await response.json();
                const data = json.data;

                if (json.status === 200) {
                    let html = '';
                    let active = '';
                    const ActiveCustomer = localStorage.getItem('ActiveCustomer');
                    const actv = JSON.parse(ActiveCustomer);

                    for (let i = 0; i < data.length; i++) {
                        if (actv) {
                            active = data[i].chat_id === actv.chat_id ? 'active' : '';
                        }

                        html += `<li class="border-bottom ${active}" id="chatid-${data[i].chat_id}"> 
                                    <a href="javascript: void(0);">
                                        <div class="d-flex align-items-center my-2">
                                            <div class="chat-user-img online align-self-center me-2 ms-0">
                                                <div class="avatar-sm">
                                                    <div class="avatar-title bg-primary rounded-circle border">
                                                        <i class="bx bxs-message-square-detail"></i>
                                                    </div>
                                                    <span class="user-status"></span>
                                                </div>
                                            </div>
                                            <div class="flex-grow-1 overflow-hidden">
                                                <span class="">${data[i].name}</span>
                                                <p class="text-truncate mb-0">${data[i].email}</p>
                                            </div>
                                            <div class="ms-auto">
                                                <span class="badge badge-soft-dark rounded p-1">${data[i].total_chat}</span>
                                            </div>
                                        </div>
                                    </a> 
                                </li>`;
                    }
                    customer_list.innerHTML = '';
                    customer_list.innerHTML += html;

                    //? On Select Customer
                    data.map((raw, index) => {
                        document.getElementById(`chatid-${raw.chat_id}`)
                        .addEventListener('click', async () => {
                            await onSelectCustomer(raw);
                        });
                    });
                }
            }

            const onSelectCustomer = async (data) => {
                localStorage.setItem('ActiveCustomer', JSON.stringify(data));
                active_name.innerHTML = data.name;
                active_status.innerHTML = "<small>online</small>";
                getCustomerList();
                getConversationData(data.chat_id, data.customer_id)
            }

            async function getConversationData(chat_id, customer_id) {
                const response = await fetch(`${url_api}/conversation.php?action=data_chat&chat_id=${chat_id}&customer_id=${customer_id}`, { 
                    method: "GET",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                });

                const json = await response.json();
                const data = json.data;

                if (json.status === 200) {
                    let html = '';
                    for (let i = 0; i < data.length; i++) {
                        if (data[i].flag_to === 'customer') {
                            html += `<li class="chat-list left">
                                    <div class="conversation-list">
                                        <div class="chat-avatar">
                                            <div class="avatar-sm">
                                                <div class="avatar-title text-primary bg-soft-primary rounded-circle border">
                                                    <i class="bx bxs-message-square-detail"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="user-chat-content">
                                            <div class="ctext-wrap">
                                                <div class="ctext-wrap-content" id="1">
                                                    <p class="mb-0 ctext-content">${data[i].message}</p>
                                                </div>
                                                <div class="dropdown align-self-start message-box-drop"> <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-vertical-rounded align-middle"></i> </a>
                                                    <div class="dropdown-menu"> <a class="dropdown-item d-flex align-items-center justify-content-between reply-message" href="#" id="reply-message-0" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between copy-message" href="#" id="copy-message-0">Copy <i class="bx bx-copy text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Mark as Unread <i class="bx bx-message-error text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between delete-item" href="#">Delete <i class="bx bx-trash text-muted ms-2"></i></a> </div>
                                                </div>
                                            </div>
                                            <div class="conversation-name">
                                                <small class="text-muted time">${data[i].date_create}</small> 
                                                <span class="text-success check-message-icon"><i class="bx bx-check-double"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </li>`;
                        } 
                        else {
                            html += `<li class="chat-list right">
                                <div class="conversation-list">
                                    <div class="user-chat-content">
                                        <div class="ctext-wrap">
                                            <div class="ctext-wrap-content">
                                                <p class="mb-0 ctext-content">${data[i].message}</p>
                                            </div>
                                            <div class="dropdown align-self-start message-box-drop"> <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-vertical-rounded align-middle"></i> </a>
                                                <div class="dropdown-menu"> <a class="dropdown-item d-flex align-items-center justify-content-between reply-message" href="#" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between copy-message" href="#">Copy <i class="bx bx-copy text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Mark as Unread <i class="bx bx-message-error text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between delete-item" href="#">Delete <i class="bx bx-trash text-muted ms-2"></i></a> </div>
                                            </div>
                                        </div>
                                        <div class="conversation-name">
                                            <small class="text-muted time">${data[i].date_create}</small> 
                                            <span class="text-success check-message-icon"><i class="bx bx-check"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </li>`;
                        }
                        
                    }
                    conversation.innerHTML = '';
                    conversation.innerHTML += html;
                }
            }

            function h() {
                var e = 12 <= (new Date).getHours() ? "pm" : "am",
                    t = 12 < (new Date).getHours() ? (new Date).getHours() % 12 : (new Date).getHours(),
                    a = (new Date).getMinutes() < 10 ? "0" + (new Date).getMinutes() : (new Date).getMinutes();
                return t < 10 ? "0" + t + ":" + a + " " + e : t + ":" + a + " " + e
            }
            