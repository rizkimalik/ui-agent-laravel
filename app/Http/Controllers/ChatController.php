<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        return view('chat');
    }

    public function sendmessage(Request $request)
    {
        $parameter = '{
            "ReqId": "'.$request->ReqId.'",
            "ReqCode": "'.$request->ReqCode.'",
            "Email": "'.$request->Email.'",
            "FromName": "'.$request->FromName.'",
            "FromToken": "'.$request->FromToken.'",
            "ToToken": "'.$request->ToToken.'",
            "FlagTo": "customer",
            "Message": "'.$request->Message.'",
            "ImageUrl": "N/A"
        }';

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://mendawai.test/api-chat/sendchat.php',
            // CURLOPT_URL => 'https://mendawai.com/menw_chat/sendchat.php',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $parameter,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
    
    public function sendmessage2(Request $request)
    {
        $parameter = '{
            "to" : "'.$request->to.'",
            "data": {
                "user_id": "' . $request->user_id . '",
                "message": "' . $request->message . '",
                "name": "' . $request->name . '",
                "email": "' . $request->email . '",
                "flag_to": "' . $request->flag_to . '",
                "date_create": "' . Carbon::now() . '",
            },
            "notification" : {
                "title" : "Message from ' . $request->name . '",
                "body" : "' . $request->message . '",
                "icon" : "assets/img/mendawai.png"
            }
        }';

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://fcm.googleapis.com/fcm/send',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $parameter,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: key=AAAAcp-eCmY:APA91bHiE0D4jbXif0VHxziR3m4A-YraV67I7994dUR35x8ruquAotwhRiBdkC2Vb6JipznBg7cBBwFxlbvdpE-Is6kDXMWiHTEhapLN4hHOIBSaOkWKTgQXXlRRlxnafLLE4l2z5MLJ'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
}
