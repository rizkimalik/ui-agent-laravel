<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ChatController;


// Route::get('/', [ChatController::class, 'index']);
// Route::get('/chat', [ChatController::class, 'index']);

Route::controller(AuthController::class)->group(function () {
    // Route::get('/', 'index');
    Route::get('/login', 'index')->name('login');
    Route::post('/login', 'authenticate');
});


// Route::middleware(['auth.session'])->group(function () {
    // Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
    Route::get('/', [ChatController::class, 'index'])->name('chat');
    Route::get('/chat', [ChatController::class, 'index']);
// });
